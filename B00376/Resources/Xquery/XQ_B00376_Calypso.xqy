xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="urn:sap-com:document:sap:idoc";

declare variable $idoc as element() external;
declare variable $responseTransco as element() external;

declare function local:convertSens($SHKZG as xs:string?) as xs:string{
        if( $SHKZG = "S")
           then 'D'
           else if($SHKZG = "H")
                then 'C'
                else ''
};

declare function local:func($idoc as element(), $responseTransco as element()) as element() {
  let $CPT_1:= if(fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BLART) = "YC")
               then $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[3]
               else if(fn:exists($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/ns1:E1FINBU[1]/ns1:KUNNR) 
                    and fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/ns1:E1FINBU[1]/ns1:KUNNR)!="") 
               then $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1] 
               else if (fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/ns1:HKONT)=("Z53101","Z51202","Z51203")) 
                    then $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[2] 
                    else $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1] ,
      
      $CPT_2:= if(fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BLART) = "YC")
               then $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[3]
               else if(fn:exists($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/ns1:E1FINBU[1]/ns1:KUNNR) 
                  and fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/ns1:E1FINBU[1]/ns1:KUNNR)!="") 
               then $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[2] 
               else if (fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/ns1:HKONT)=("Z53101","Z51202","Z51203")) 
                    then $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1] 
                    else $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[2],
      
      $PE3:= if(fn:exists($CPT_1/ns1:E1FINBU[1]/ns1:KUNNR) and fn:data($CPT_1/ns1:E1FINBU[1]/ns1:KUNNR)!="") 
             then "" 
             else fn:data($CPT_1/ns1:HKONT),
             
      $PE5:= if(fn:exists($CPT_2/ns1:E1FINBU[1]/ns1:KUNNR) and fn:data($CPT_2/ns1:E1FINBU[1]/ns1:KUNNR)!="") 
             then "" 
             else fn:data($CPT_2/ns1:HKONT),
      
      $PE6:= if(fn:exists($CPT_1/ns1:E1FINBU[1]/ns1:KUNNR) 
                and fn:data($CPT_1/ns1:E1FINBU[1]/ns1:KUNNR)!=""
                and fn:exists($CPT_1/ns1:E1FINBU[1]/ns1:ZLSCH)) 
             then fn:data($CPT_1/ns1:E1FINBU[1]/ns1:ZLSCH)
             else "",
      $KUNNR:= if(fn:exists($CPT_1/ns1:E1FINBU[1]/ns1:KUNNR) and fn:data($CPT_1/ns1:E1FINBU[1]/ns1:KUNNR)!="") 
                then fn:data($CPT_1/ns1:E1FINBU[1]/ns1:KUNNR)
                else if(fn:exists($CPT_2/ns1:E1FINBU[1]/ns1:KUNNR) and fn:data($CPT_2/ns1:E1FINBU[1]/ns1:KUNNR)!="") 
                    then fn:data($CPT_2/ns1:E1FINBU[1]/ns1:KUNNR)
                    else "",
      $ECRIT_CAISSE:= $responseTransco/*:ET_DATA[./*:CODE_TRANSCO="ECRIT_CAISSE"
                                                        and ./*:PE1=fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BLART) 
                                                        and ./*:PE2=fn:data($CPT_1/ns1:BSCHL) 
                                                        and ./*:PE3=$PE3
                                                        and ./*:PE4=fn:data($CPT_2/ns1:BSCHL)
                                                        and ./*:PE5=$PE5
                                                        and ./*:PE6=$PE6][1],
      $CODE_MVT:= fn:data($ECRIT_CAISSE/*:PS1),
      $MODE_PAY:= fn:data($ECRIT_CAISSE/*:PS2)
  return
    <FIDCCP02>
      <IDOC>
        <E1FIKPF>
          <PROCESS_NAME>B0037602</PROCESS_NAME>
          <DOCNUM>{fn:data($idoc/ns1:IDOC[1]/ns1:EDI_DC40/ns1:DOCNUM)}</DOCNUM>
          <BUSINESS_UNIT>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BUKRS)}</BUSINESS_UNIT>
          <PAYMENT_ID>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BELNR)}</PAYMENT_ID>
          {
          if(fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:Z1E1FIKPF01[1]/ns1:ZZLET_FLAG)='L' 
             or (fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:Z1E1FIKPF01[1]/ns1:ZZLET_FLAG)='D'
                  and fn:exists($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BKTXT) 
                  and fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BKTXT)!=""))
          then <CODE_MVT></CODE_MVT>
          else <CODE_MVT>{$CODE_MVT}</CODE_MVT>
          }
          
          <ENTRY_DT>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BLDAT)}</ENTRY_DT>
          
          {let $werks := fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[fn:exists(./ns1:WERKS) and fn:data(./ns1:WERKS)!=''][1]/ns1:WERKS)
            return
          if( $werks != '' ) 
          then  <SUBCUST_QUAL1>{$werks}</SUBCUST_QUAL1>
          else  <SUBCUST_QUAL1>{fn:substring(fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:XBLNR),1,4)}</SUBCUST_QUAL1>
          }
          <SG_RAPPRO>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BKTXT)}</SG_RAPPRO>
          <BILL_OF_LADING>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:XREF1_HD)}</BILL_OF_LADING>
          <ZPS_ITEM>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:Z1E1FIKPF01[1]/ns1:ZZPSITEM)}</ZPS_ITEM> 
          <ZPS_ITEM2>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:Z1E1FIKPF01[1]/ns1:ZZPSITEM2)}</ZPS_ITEM2>
          {
          if(fn:exists($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BKTXT) and fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BKTXT)!="")
          then <SG_LETTRAGE>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:Z1E1FIKPF01[1]/ns1:ZZLET_FLAG)}</SG_LETTRAGE>
          
          else <SG_LETTRAGE/>
          }
          <BAL_AMT>{fn:data($CPT_1/ns1:WRBTR)}</BAL_AMT>
          <DESC>{substring-before(fn:data($CPT_1/ns1:SGTXT),'/')}</DESC>
          <CUST_ID>{fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:Z1E1FIKPF01[1]/ns1:EIKTO)}</CUST_ID>
         
          
          {
           if(  $KUNNR !="" )
            then        
                  if($KUNNR ="PART_OCCAS" or $KUNNR ="PRO_OCCAS" )
                  then <ORIGIN>G</ORIGIN>
                  else <ORIGIN>S</ORIGIN>
          
          else <ORIGIN/>}
          
          {if(fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BLART) != "YD")
          then <IMMEDIAT>O</IMMEDIAT>
          else <IMMEDIAT>N</IMMEDIAT>}
          
          <SG_DATE_RAPPRO>{fn:data($CPT_1/ns1:AUGDT)}</SG_DATE_RAPPRO>
          
          {let $SHKZG_1:= $idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[./ns1:HKONT/text()="Z41100"][1]/ns1:SHKZG,
               $SHKZG_2:= $idoc/ns1:IDOC[1]/ns1:E1FIKPF[fn:data(./ns1:BLART)=("ZJ","DZ")][1]/ns1:E1FISEG[fn:data(./ns1:HKONT)="Z51100"][1]/ns1:SHKZG,
               $SHKZG_3:= $idoc/ns1:IDOC[1]/ns1:E1FIKPF[fn:data(./ns1:BLART)=("YC")][1]/ns1:E1FISEG[fn:data(./ns1:HKONT)=("Z75801","Z65801") ][1]/ns1:SHKZG,
               $SHKZG_4:= $idoc/ns1:IDOC[1]/ns1:E1FIKPF[fn:data(./ns1:BLART)=("ZP")][1]/ns1:E1FISEG[fn:data(./ns1:HKONT)="Z53101"][1]/ns1:SHKZG
          return
          if(fn:exists($SHKZG_1) and fn:data($SHKZG_1)!="") then
            <SENS>{local:convertSens(fn:data($SHKZG_1))}</SENS>
           else if(fn:exists($SHKZG_2) and fn:data($SHKZG_2)!="") then
            <SENS>{local:convertSens(fn:data($SHKZG_2))}</SENS>
           else if(fn:exists($SHKZG_3) and fn:data($SHKZG_3)!="") then
            <SENS>{local:convertSens(fn:data($SHKZG_3))}</SENS>
           else if(fn:exists($SHKZG_4) and fn:data($SHKZG_4)!="") then
            <SENS>{local:convertSens(fn:data($SHKZG_4))}</SENS>
           else <SENS>{local:convertSens(fn:data($CPT_1/ns1:SHKZG))}</SENS>
           
          }
          
          <PAYMENT_METHOD>{$MODE_PAY}</PAYMENT_METHOD>
          <SG_REMIX/>
          
          {
          if(fn:data($CPT_2/ns1:HKONT)="Z53101" and $CODE_MVT != "320" and $CODE_MVT != "321" )
          then ()
            
          else <BANK_CD>{
          let $BUKRS_PE1:=fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:BUKRS)
          let $XBLNR_PE2:=fn:substring(fn:data($idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:XBLNR),1,4) 
          let $HKONT_PE3:=fn:data($CPT_2/ns1:HKONT)
          let $ZLSCH_PE4:=fn:data($CPT_1/ns1:E1FINBU[1]/ns1:ZLSCH)
          return
          
          if($XBLNR_PE2 ne "") then
          
            if($ZLSCH_PE4 ne "") then
             
             (: cas ou tous les PE1,PE2,PE3,PE4 sont renseignes et correspondent a une donnee dans la table de transcos:)
              if(fn:exists($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                    and ./*:PE1=$BUKRS_PE1
                    and ./*:PE2=$XBLNR_PE2
                    and ./*:PE3=$HKONT_PE3
                    and ./*:PE4=$ZLSCH_PE4])) then
                      fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                    and ./*:PE1=$BUKRS_PE1
                    and ./*:PE2=$XBLNR_PE2
                    and ./*:PE3=$HKONT_PE3
                    and ./*:PE4=$ZLSCH_PE4][1]/*:PS1)
               else
               (:aucune donne trouvee => on cherche avec trois parametres PE1,PE2,PE3:)
                   if(fn:exists($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                    and ./*:PE1=$BUKRS_PE1
                    and ./*:PE2=$XBLNR_PE2
                    and ./*:PE3=$HKONT_PE3
                    and ./*:PE4=""
                    ])) then
                      fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                    and ./*:PE1=$BUKRS_PE1
                    and ./*:PE2=$XBLNR_PE2
                    and ./*:PE3=$HKONT_PE3
                    and ./*:PE4=""
                    ][1]/*:PS1)
                   
                    else
                    (:aucune donne trouvee => on cherche avec trois parametres PE1,PE3,PE4:)
                       if(fn:exists($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                        and ./*:PE1=$BUKRS_PE1 
                        and ./*:PE3=$HKONT_PE3
                        and ./*:PE4=$ZLSCH_PE4
                        and ./*:PE2=""
                        ])) then
                        fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                        and ./*:PE1=$BUKRS_PE1
                        and ./*:PE3=$HKONT_PE3
                        and ./*:PE4=$ZLSCH_PE4
                        and ./*:PE2=""
                        ][1]/*:PS1)
                    else
                    (:aucune donne trouvee => on cherche avec trois parametres PE1,PE2:)
                        fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                        and ./*:PE1=$BUKRS_PE1
                        and ./*:PE3=$HKONT_PE3
                        and ./*:PE2=""
                        and ./*:PE4=""
                        ][1]/*:PS1
                        
                        )
                    
                    
                (: cas $ZLSCH_PE4 vide:)
                else
                 if(fn:exists($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                    and ./*:PE1=$BUKRS_PE1
                    and ./*:PE2=$XBLNR_PE2
                    and ./*:PE3=$HKONT_PE3
                    and ./*:PE4=""
                    ])) then
                      fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                    and ./*:PE1=$BUKRS_PE1
                    and ./*:PE2=$XBLNR_PE2
                    and ./*:PE3=$HKONT_PE3
                    and ./*:PE4=""
                    ][1]/*:PS1) 
                  else
                     fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                        and ./*:PE1=$BUKRS_PE1
                        and ./*:PE3=$HKONT_PE3
                        and ./*:PE2=""
                        and ./*:PE4=""
                        ][1]/*:PS1)
          
            else
             (: cas $XBLNR_PE2 vide :)
                       if(fn:exists($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                        and ./*:PE1=$BUKRS_PE1 
                        and ./*:PE3=$HKONT_PE3
                        and ./*:PE4=$ZLSCH_PE4
                        and ./*:PE2=""
                        ])) then
                        fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                        and ./*:PE1=$BUKRS_PE1
                        and ./*:PE3=$HKONT_PE3
                        and ./*:PE4=$ZLSCH_PE4
                        and ./*:PE2=""
                        ][1]/*:PS1)
                    else
                        fn:data($responseTransco/*:ET_DATA[./*:CODE_TRANSCO="BANQUE_CAISS"
                        and ./*:PE1=$BUKRS_PE1
                        and ./*:PE3=$HKONT_PE3
                        and ./*:PE2=""
                        and ./*:PE4=""
                        ][1]/*:PS1)
                        
          }</BANK_CD>
          }
          
          <DUE_DT>{$idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/*:VALUT/text()}</DUE_DT>
          <NUMGARANTIE>{$idoc/ns1:IDOC[1]/ns1:E1FIKPF[1]/ns1:E1FISEG[1]/ns1:XREF3/text()}</NUMGARANTIE>
        </E1FIKPF>
      </IDOC>
    </FIDCCP02>
};

local:func($idoc,$responseTransco)
