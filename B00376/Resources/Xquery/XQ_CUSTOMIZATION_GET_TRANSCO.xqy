xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/gettranscos.apimanagment";
(:: import schema at "Schemas/BS_ZCL_API_CUSTOMIZATIONS01_REST.wsdl" ::)

declare variable $IV_TARGET_SYS as xs:string ? external;
declare variable $IV_SOURCE_SYS as xs:string ? external;
declare variable $IV_TRANSCO_CD as xs:string ? external;

declare function local:func($IV_TARGET_SYS as xs:string ?, 
                            $IV_SOURCE_SYS as xs:string ?, 
                            $IV_TRANSCO_CD as xs:string ?) 
                            as element() (:: schema-element(ns1:getTranscosByCode_params) ::) {
    <ns1:getTranscosByCode_params>
    {
    if($IV_SOURCE_SYS ne "") then
    <ns1:IV_SOURCE_SYS>{$IV_SOURCE_SYS}</ns1:IV_SOURCE_SYS>
    
    else
    ()
    }
    {
    if($IV_TARGET_SYS ne "") then
    <ns1:IV_TARGET_SYS>{$IV_TARGET_SYS}</ns1:IV_TARGET_SYS>
    
    else
    ()
    }
    {
    if($IV_TRANSCO_CD ne "") then
    <ns1:IV_TRANSCO_CD>{$IV_TRANSCO_CD}</ns1:IV_TRANSCO_CD>
    
    else
    ()
    }
    
    
    </ns1:getTranscosByCode_params>
};

local:func($IV_TARGET_SYS, $IV_SOURCE_SYS, $IV_TRANSCO_CD)
