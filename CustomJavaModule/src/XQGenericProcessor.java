import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;

import javax.xml.namespace.QName;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQConstants;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQItemType;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQSequence;
import javax.xml.xquery.XQStaticContext;

import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLParseException;
import oracle.xml.xquery.OXQConnection;
import oracle.xml.xquery.OXQDataSource;
import oracle.xml.xquery.OXQEntity;
import oracle.xml.xquery.OXQEntityKind;
import oracle.xml.xquery.OXQEntityLocator;
import oracle.xml.xquery.OXQEntityResolver;
import oracle.xml.xquery.OXQEntityResolverRequestOptions;
import oracle.xml.xquery.OXQView;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XQGenericProcessor {
	private static class MyEntityResolver extends OXQEntityResolver {
        @Override
        public OXQEntity resolveEntity(OXQEntityKind kind, OXQEntityLocator locator,
                OXQEntityResolverRequestOptions options) throws IOException {
            if (kind == OXQEntityKind.MODULE) {
                URI systemId = locator.getSystemIdAsURI();
                if (systemId != null && "file".equals(systemId.getScheme())) {
                    File file = new File(systemId);
                    FileInputStream input = new FileInputStream(file);
                    OXQEntity result = new OXQEntity(input);
                    result.enlistCloseable(input);
                    return result;
                }
            }
            return null;
        }
    }

	public static String CallXqWithGeneric(String inputRequest,String elementName,String elemType,String queryPath,boolean xqWithXQcheck) throws XQException, IOException {
			
			inputRequest=inputRequest.replaceAll("<([a-z0-9A-Z]*):","<");
			inputRequest=inputRequest.replaceAll("</([a-z0-9A-Z]*):","</");
			inputRequest=inputRequest.replaceAll("xmlns:([/a-z0-9]*)=","xmlns=");
			
		 	OXQDataSource ds = new OXQDataSource();
	    	XQConnection con = ds.getConnection();
	    	if(xqWithXQcheck){
	    		OXQConnection ocon = OXQView.getConnection(con);
		        ocon.setEntityResolver(new MyEntityResolver());
	    	}

	    	XQStaticContext ctx = con.getStaticContext();
	    	ctx.setBindingMode(XQConstants.BINDING_MODE_DEFERRED);
	    	con.setStaticContext(ctx);
	    	
//	    	System.out.println("###Input"+inputRequest.length());
	    	String[] input = inputRequest.split("\\|\\|\\|\\|"); // Split function is used to split inputRequest,elementName,elementType it can be use
	    	String[] elementNode = elementName.split("\\|\\|");
	    	String[] elementType = elemType.split("\\|\\|");

	    	FileInputStream query = null;
	    	XQPreparedExpression expr = null;
	    	//XqWithXQcheck is used to check if there is module load in the XQuery with another xquery call in the after the query expression is prepared
	    	if(xqWithXQcheck){
	    		File queryfile = new File(queryPath);
	    		ctx.setBaseURI(queryfile.toURI().toString());
	    		query = new FileInputStream(queryfile);
	    		expr = con.prepareExpression(query,ctx);
	    	}
	    	else{
	    		query = new FileInputStream(queryPath);
	    		expr = con.prepareExpression(query);
	    	}	
	    	
	    	query.close();
	    	
	    	String elementCheck = "element()";
	    	//In these try block the binding of the Input request is done with the respective X-query external variable
	    	try {
	    		for ( int i=0;i<input.length;i++){	    				
	    				if(elementType[i].equals(elementCheck)){
	    					
	    					
	    					/*System.out.println("####Input"+input[i]+"Lenght"+input);
	    					System.out.println("####Element"+elementType[i]);
	    					System.out.println("####Element Node"+elementNode[i]);*/
			    			DOMParser docParser = new DOMParser();
			    			docParser.parse(new InputSource(new java.io.StringReader(input[i])));
			    			XMLDocument doc = new XMLDocument();
			    			doc = docParser.getDocument();
			    			XMLElement docElem = (XMLElement) doc.getDocumentElement();
			    			XQItemType itemTypedoc = con.createElementType(new QName(docElem.getNamespaceURI(),docElem.getTagName()), XQItemType.XQBASETYPE_UNTYPED);
			    			expr.bindNode(new QName(elementNode[i]), docElem, itemTypedoc);
	    				}
	    				else{
	    					/*
	    					System.out.println("####Input"+input[i]);
	    					System.out.println("####Element else"+elementType[i]);
	    					System.out.println("####Element Node else"+elementNode[i]);*/
	    					
	    					 XQItemType stringType = con.createAtomicType(XQItemType.XQBASETYPE_STRING);
	    					 if(input[i] != "null"){
	    						 expr.bindString(new QName(elementNode[i]),input[i],stringType);
	    					 }	 
	    					 else{
	    						 expr.bindString(new QName(elementNode[i]),"",stringType);
	    					 }
	    				}
	    			}
	    		} catch (XMLParseException e) {
	    			} 
	    			catch (SAXException e) {
	    			}
	    	//In these below the prepared expression is executed and the out result is store and return to the Java invoke call
	    	XQSequence result = expr.executeQuery();
	    	String resultOutput=(result.getSequenceAsString(null));
	    	System.out.println(resultOutput);

	    	result.close();
	    	expr.close();
	    	con.close();
	    	return resultOutput;
	    }
}