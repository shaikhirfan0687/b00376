xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://sgbdf.saint-gobain.com/Generic/RoutingCtx/1";
(:: import schema at "Schemas/XSD_ROUTING_V1.xsd" ::)

declare variable $businessObject as xs:string external;
declare variable $businessID as xs:string external;
declare variable $format as xs:string external;
declare variable $version as xs:string external;
declare variable $messageID as xs:string external;
declare variable $source as xs:string external;
declare variable $target as xs:string external;
declare variable $timestamp as xs:string external;
declare variable $pipeline as xs:string external;

declare function local:funcSetRoutingCtx($businessObject as xs:string, $businessID as xs:string, $format as xs:string, $version as xs:string, $messageID as xs:string, $source as xs:string, $target as xs:string, $timestamp as xs:string,$pipeline as xs:string) as element() (:: schema-element(ns1:routingctx) ::) {
    <ns1:routingctx>
        <ns1:businessObject>{fn:data($businessObject)}</ns1:businessObject>
        <ns1:businessID>{fn:data($businessID)}</ns1:businessID>
        <ns1:format>{fn:data($format)}</ns1:format>
        <ns1:version>{fn:data($version)}</ns1:version>
        <ns1:messageID>{fn:data($messageID)}</ns1:messageID>
        <ns1:source>{fn:data($source)}</ns1:source>
        <ns1:target>{fn:data($target)}</ns1:target>
        <ns1:timestamp>{fn:data($timestamp)}</ns1:timestamp>
        <ns1:pipeline>{fn:data($pipeline)}</ns1:pipeline>
    </ns1:routingctx>
};

local:funcSetRoutingCtx($businessObject, $businessID, $format, $version, $messageID, $source, $target, $timestamp,$pipeline)
